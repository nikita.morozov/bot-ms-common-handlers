package module

import (
	"gitlab.com/nikita.morozov/bot-ms-common-handlers/handlers"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type ExportModuleHandler struct {
}

func (g *ExportModuleHandler) GetName() string {
	return "Core module - v1.4.2"
}

func (g *ExportModuleHandler) GetHandlers() []models.ModuleHandler {
	return []models.ModuleHandler{
		handlers.NewAddToObjectHandler(),
		handlers.NewConditionHandler(),
		handlers.NewCutStringHandler(),
		handlers.NewGenerateAlphaCodeHandler(2, 10),
		handlers.NewGetCharHandler(),
		handlers.NewPrefixStringHandler(),
		handlers.NewRouterHandler(8),
		handlers.NewSeparateHandler(),
		handlers.NewSuffixStringHandler(),
		handlers.NewPrecisionConvertHandler(),
		handlers.NewCheckStartWithHandler(),
		handlers.NewCheckStringLengthHandler(),
		handlers.NewCompareNumberHandler(),
		handlers.NewStringToIntHandler(),
		handlers.NewAddIntToObjectHandler(),
		handlers.NewAddObjectToObjectHandler(),
		handlers.NewTemplateStringHandler(),
		handlers.NewBase64EncodeHandler(),
		handlers.NewBase64DecodeHandler(),
	}
}

// Do not update old seeders records

func (g *ExportModuleHandler) GetSeeds() []models.ModuleSeed {
	return []models.ModuleSeed{
		{
			Name: "initial_core_seeds",
			Actions: []models.Action{
				handlers.NewAddToObjectAction(),
				handlers.NewConditionAction(),
				handlers.NewCutStringAction(),
				handlers.NewGenerateAlphaCodeAction(),
				handlers.NewPrefixStringAction(),
				handlers.NewRouterAction(),
				handlers.NewSeparateAction(),
				handlers.NewSeparateAction(),
				handlers.NewSuffixStringAction(),
			},
		},
		{
			Name: "precision_seeds",
			Actions: []models.Action{
				handlers.NewPrecisionConvertAction(),
			},
		},
		{
			Name: "string_tools_1",
			Actions: []models.Action{
				handlers.NewCheckStringLengthAction(),
				handlers.NewCheckStartWithAction(),
			},
		},
		{
			Name: "core_seeds1",
			Actions: []models.Action{
				handlers.NewCompareNumberAction(),
				handlers.NewStringToIntAction(),
			},
		},
		{
			Name: "added_int_add_object",
			Actions: []models.Action{
				handlers.NewAddIntToObjectAction(),
			},
		},
		{
			Name: "added_object_add_object",
			Actions: []models.Action{
				handlers.NewAddObjectToObjectAction(),
			},
		},
		{
			Name: "added_template_string",
			Actions: []models.Action{
				handlers.NewTemplateStringAction(),
			},
		},
		{
			Name: "base64",
			Actions: []models.Action{
				handlers.NewBase64DecodeAction(),
				handlers.NewBase64EncodeAction(),
			},
		},
	}
}
