module gitlab.com/nikita.morozov/bot-ms-common-handlers

go 1.18

require (
	github.com/stretchr/testify v1.7.0
	github.com/tidwall/gjson v1.14.0
	gitlab.com/nikita.morozov/bot-ms-core v1.4.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	github.com/tidwall/sjson v1.2.4 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
