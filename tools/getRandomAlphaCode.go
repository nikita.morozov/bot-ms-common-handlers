package tools

import (
	"math/rand"
	"time"
)

func GetRandomAlphaCode(count int) string {
	var letterRunes = []rune("1234567890")

	var result string

	for i := 0; i < count; i++ {
		rand.Seed(time.Now().UTC().UnixNano())
		index := rand.Intn(len(letterRunes))
		letter := letterRunes[index]
		result += string(letter)
	}

	return result
}