package tools

import (
	"fmt"
)

// Log - Simple full colorable message
// LogN - Simple full colorable log with extra top indent
// LogD - Log with two parts - colorable and white
// LogDN - Log with two parts - colorable and white, and with extra top indent

// RED

func RedLog(msg string) {
	fmt.Printf("\n\x1b[31m%s\x1b[0m", msg)
}

func RedLogN(msg string) {
	fmt.Printf("\n\n\x1b[31m%s\x1b[0m", msg)
}

func RedLogD(msg1 string, msg2 interface{}) {
	fmt.Printf("\n\x1b[31m%s\x1b[0m%v", msg1, msg2)
}

func RedLogDN(msg1 string, msg2 interface{}) {
	fmt.Printf("\n\n\x1b[31m%s\x1b[0m%v", msg1, msg2)
}

// GREEN

func GreenLog(msg string) {
	fmt.Printf("\n\x1b[32m%s\x1b[0m", msg)
}

func GreenLogN(msg string) {
	fmt.Printf("\n\n\x1b[32m%s\x1b[0m", msg)
}

func GreenLogD(msg1 string, msg2 interface{}) {
	fmt.Printf("\n\x1b[32m%s\x1b[0m%v", msg1, msg2)
}

func GreenLogDN(msg1 string, msg2 interface{}) {
	fmt.Printf("\n\n\x1b[32m%s\x1b[0m%v", msg1, msg2)
}

// YELLOW

func YellowLog(msg string) {
	fmt.Printf("\n\x1b[33m%s\x1b[0m", msg)
}

func YellowLogN(msg string) {
	fmt.Printf("\n\n\x1b[33m%s\x1b[0m", msg)
}

func YellowLogD(msg1 string, msg2 interface{}) {
	fmt.Printf("\n\x1b[33m%s\x1b[0m%v", msg1, msg2)
}

func YellowLogDN(msg1 string, msg2 interface{}) {
	fmt.Printf("\n\n\x1b[33m%s\x1b[0m%v", msg1, msg2)
}

// BLUE

func BlueLog(msg string) {
	fmt.Printf("\n\x1b[34m%s\x1b[0m", msg)
}

func BlueLogN(msg string) {
	fmt.Printf("\n\n\x1b[34m%s\x1b[0m", msg)
}

func BlueLogD(msg1 string, msg2 interface{}) {
	fmt.Printf("\n\x1b[34m%s\x1b[0m%v", msg1, msg2)
}

func BlueLogDN(msg1 string, msg2 interface{}) {
	fmt.Printf("\n\n\x1b[34m%s\x1b[0m%v", msg1, msg2)
}

// MAGENTA

func MagentaLog(msg string) {
	fmt.Printf("\n\x1b[35m%s\x1b[0m", msg)
}

func MagentaLogN(msg string) {
	fmt.Printf("\n\n\x1b[35m%s\x1b[0m", msg)
}

func MagentaLogD(msg1 string, msg2 interface{}) {
	fmt.Printf("\n\x1b[35m%s\x1b[0m%v", msg1, msg2)
}

func MagentaLogDN(msg1 string, msg2 interface{}) {
	fmt.Printf("\n\n\x1b[35m%s\x1b[0m%v", msg1, msg2)
}

// CYAN

func CyanLog(msg string) {
	fmt.Printf("\n\x1b[36m%s\x1b[0m", msg)
}

func CyanLogN(msg string) {
	fmt.Printf("\n\n\x1b[36m%s\x1b[0m", msg)
}

func CyanLogD(msg1 string, msg2 interface{}) {
	fmt.Printf("\n\x1b[36m%s\x1b[0m%v", msg1, msg2)
}

func CyanLogDN(msg1 string, msg2 interface{}) {
	fmt.Printf("\n\n\x1b[36m%s\x1b[0m%v", msg1, msg2)
}

// GRAY

func GrayLog(msg string) {
	fmt.Printf("\n\x1b[37m%s\x1b[0m", msg)
}

func GrayLogN(msg string) {
	fmt.Printf("\n\n\x1b[37m%s\x1b[0m", msg)
}

func GrayLogD(msg1 string, msg2 interface{}) {
	fmt.Printf("\n\x1b[37m%s\x1b[0m%v", msg1, msg2)
}

func GrayLogDN(msg1 string, msg2 interface{}) {
	fmt.Printf("\n\n\x1b[37m%s\x1b[0m%v", msg1, msg2)
}

// WHITE

func WhiteLog(msg string) {
	fmt.Printf("\n\x1b[0m%v", msg)
}

func WhiteLogN(msg string) {
	fmt.Printf("\n\n\x1b[0m%v", msg)
}
