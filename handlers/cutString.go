package handlers

import (
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"strconv"
)

type CutStringHandler struct {
}

func (c *CutStringHandler) Execute(params models.NodeParams) models.NodeData {
	startInt := params.GetInt("start")
	endInt := params.GetInt("end")
	value := params.GetString("value")

	start, startErr := strconv.ParseUint(strconv.Itoa(int(startInt)), 10, 32)
	end, endErr := strconv.ParseUint(strconv.Itoa(int(endInt)), 10, 32)

	if startErr != nil || endErr != nil || value == "" {
		return models.NodeDataError("START INDEX OR END INDEX OR VALUE IS NULL OR EMPTY")
	}

	runeValue := []rune(value)

	result := string(runeValue[start : len(value)-int(end)])

	resp := models.NodeData{}

	resp.Set("outValue", result)

	return resp.Complete(nil)
}

const NewCutStringHandlerName = "core.cutString"

func NewCutStringHandler() models.ModuleHandler {
	return models.ModuleHandler{
		Name:    NewCutStringHandlerName,
		Handler: &CutStringHandler{},
	}
}

func NewCutStringAction() models.Action {
	return models.Action{
		Name:    "Core | CutString",
		Handler: NewCutStringHandlerName,
		Sockets: []models.Socket{
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeEvent,
				Title:     "OnStart",
				Slug:      models.NodeDataOnStart,
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeInt,
				Title:     "Start index",
				Slug:      "start",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeInt,
				Title:     "End index",
				Slug:      "end",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Value",
				Slug:      "value",
			},
			// Out
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnComplete,
				Slug:      models.NodeDataOnComplete,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnError,
				Slug:      models.NodeDataOnError,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeString,
				Title:     "Value",
				Slug:      "outValue",
			},
		},
	}
}
