package handlers

import (
	"fmt"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type RouterHandler struct {
	maxRoutesCount int
}

func (r *RouterHandler) Execute(params models.NodeParams) models.NodeData {
	value := params.GetString("value")

	if value == "" {
		return models.NodeDataError("VALUE IS NULL OR EMPTY")
	}

	for i := 0; i < r.maxRoutesCount; i++ {
		routeName := fmt.Sprintf("route%d", i)
		routeValue := params.GetString(routeName)

		if value == routeValue {
			resp := models.NodeData{}
			resp.SetEvent(fmt.Sprintf("resultRoute%d", i))
			return resp
		}
	}

	return models.NodeDataError("router-handler.route.to-found")
}

const RouterHandlerName = "core.router"

func NewRouterHandler(maxRoutesCount int) models.ModuleHandler {
	return models.ModuleHandler{
		Name: RouterHandlerName,
		Handler: &RouterHandler{
			maxRoutesCount: maxRoutesCount,
		},
	}
}

func NewRouterAction() models.Action {
	return models.Action{
		Name:    "Core | Router",
		Handler: RouterHandlerName,
		Sockets: []models.Socket{
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeEvent,
				Title:     "OnStart",
				Slug:      models.NodeDataOnStart,
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Value",
				Slug:      "value",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Route1",
				Slug:      "route0",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Route2",
				Slug:      "route1",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Route3",
				Slug:      "route2",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Route4",
				Slug:      "route3",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Route5",
				Slug:      "route4",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Route6",
				Slug:      "route5",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Route7",
				Slug:      "route6",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Route8",
				Slug:      "route7",
			},
			// Out
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnError,
				Slug:      models.NodeDataOnError,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     "On Route 1",
				Slug:      "resultRoute0",
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     "On Route 2",
				Slug:      "resultRoute1",
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     "On Route 3",
				Slug:      "resultRoute2",
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     "On Route 4",
				Slug:      "resultRoute3",
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     "On Route 5",
				Slug:      "resultRoute4",
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     "On Route 6",
				Slug:      "resultRoute5",
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     "On Route 7",
				Slug:      "resultRoute6",
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     "On Route 8",
				Slug:      "resultRoute7",
			},
		},
	}
}
