package handlers

import (
	"encoding/base64"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type Base64DecodeHandler struct {
}

func (c *Base64DecodeHandler) Execute(params models.NodeParams) models.NodeData {
	value := params.GetString("value")

	decoded, err := base64.StdEncoding.DecodeString(value)
	if err != nil {
		return models.NodeDataError(err.Error())
	}

	resp := models.NodeData{}
	resp.Set("outValue", string(decoded))
	return resp.Complete(nil)
}

const NewBase64DecodeHandlerName = "core.base64Decode"

func NewBase64DecodeHandler() models.ModuleHandler {
	return models.ModuleHandler{
		Name:    NewBase64DecodeHandlerName,
		Handler: &Base64DecodeHandler{},
	}
}

func NewBase64DecodeAction() models.Action {
	return models.Action{
		Name:    "Coder | Base64Decode",
		Handler: NewBase64DecodeHandlerName,
		Sockets: []models.Socket{
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeEvent,
				Title:     "OnStart",
				Slug:      models.NodeDataOnStart,
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Value",
				Slug:      "value",
			},
			// Out
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnComplete,
				Slug:      models.NodeDataOnComplete,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnError,
				Slug:      models.NodeDataOnError,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeString,
				Title:     "Value",
				Slug:      "outValue",
			},
		},
	}
}
