package handlers

import (
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type CompareNumberHandler struct {
}

func (c *CompareNumberHandler) Execute(params models.NodeParams) models.NodeData {
	value1 := params.GetInt("value1")
	value2 := params.GetInt("value2")
	comparison := params.GetString("comparison")

	resp := models.NodeData{}

	res := false
	switch comparison {
	case "<":
		res = value1 < value2
		break
	case ">":
		res = value1 > value2
		break
	case "<=":
		res = value1 <= value2
		break
	case ">=":
		res = value1 >= value2
		break
	case "=":
	case "==":
		res = value1 == value2
		break
	default:
		return models.NodeDataError("comparison-operator-not-found")
	}

	if res {
		resp.SetEvent("true")
	} else {
		resp.SetEvent("false")
	}

	return resp.Complete(nil)
}

const NewCompareNumberHandlerName = "core.compareNumber"

func NewCompareNumberHandler() models.ModuleHandler {
	return models.ModuleHandler{
		Name:    NewCompareNumberHandlerName,
		Handler: &CompareNumberHandler{},
	}
}

func NewCompareNumberAction() models.Action {
	return models.Action{
		Name:    "Core | Compare numbers",
		Handler: NewCompareNumberHandlerName,
		Sockets: []models.Socket{
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeEvent,
				Slug:      models.NodeDataOnStart,
				Title:     "OnStart",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeInt,
				Title:     "Value A",
				Slug:      "value1",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeInt,
				Title:     "Value B",
				Slug:      "value2",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Sign (<, =, ...)",
				Slug:      "comparison",
			},
			// Out
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     "OnTrue",
				Slug:      "true",
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     "OnFalse",
				Slug:      "false",
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnError,
				Slug:      models.NodeDataOnError,
			},
		},
	}
}
