package handlers

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"strconv"
	"testing"
)

func Test_CutString(t *testing.T) {
	start := 5
	end := 3
	value := "this_value_for_testing"

	handler := NewCutStringHandler()

	mockPostValues := []byte("value_for_test")

	data := models.NodeParams{
		"start": []byte(strconv.Itoa(start)),
		"end":   []byte(strconv.Itoa(end)),
		"value": []byte(value),
	}

	postValues := handler.Handler.Execute(data)

	assert.Equal(t, mockPostValues, postValues["outValue"])

	fmt.Printf("\n\x1b[32mCut String Test - Success\n")
}
