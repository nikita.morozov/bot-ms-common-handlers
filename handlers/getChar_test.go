package handlers

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"strconv"
	"testing"
)

func Test_GetChar(t *testing.T) {
	index := 2
	value := "1 +"

	handler := NewGetCharHandler()

	mockPostValues := []byte("+")

	data := models.NodeParams{
		"index": []byte(strconv.Itoa(index)),
		"value": []byte(value),
	}

	postValues := handler.Handler.Execute(data)

	assert.Equal(t, mockPostValues, postValues["outValue"])

	fmt.Printf("\n\x1b[32mGet Char Test - Success\n")
}
