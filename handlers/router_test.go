package handlers

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"testing"
)

func Test_Router(t *testing.T) {
	value := "value2"
	route1 := "value1"
	route2 := "value2"
	route3 := "value3"

	handler := NewRouterHandler(3)

	data := models.NodeParams{
		"value":  []byte(value),
		"route1": []byte(route1),
		"route2": []byte(route2),
		"route3": []byte(route3),
	}

	resp := handler.Handler.Execute(data)

	assert.Equal(t, "true", resp.GetString("resultRoute2"))

	fmt.Printf("\n\x1b[32mRouter Test - Success\n")
}
