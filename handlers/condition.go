package handlers

import (
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type ConditionHandler struct {
}

func (c *ConditionHandler) Execute(params models.NodeParams) models.NodeData {
	a := params.GetString("a")
	b := params.GetString("b")

	resp := models.NodeData{}

	if a == b {
		resp.SetEvent("true")
	} else {
		resp.SetEvent("false")
	}

	return resp.Complete(nil)
}

const NewConditionHandlerName = "core.equalCondition"

func NewConditionHandler() models.ModuleHandler {
	return models.ModuleHandler{
		Name:    NewConditionHandlerName,
		Handler: &ConditionHandler{},
	}
}

func NewConditionAction() models.Action {
	return models.Action{
		Name:    "Core | Equal condition",
		Handler: NewConditionHandlerName,
		Sockets: []models.Socket{
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeEvent,
				Title:     "OnStart",
				Slug:      models.NodeDataOnStart,
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "A value",
				Slug:      "a",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "B Value",
				Slug:      "b",
			},
			// Out
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnComplete,
				Slug:      models.NodeDataOnComplete,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnError,
				Slug:      models.NodeDataOnError,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     "OnTrue",
				Slug:      "true",
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     "OnFalse",
				Slug:      "false",
			},
		},
	}
}
