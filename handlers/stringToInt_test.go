package handlers

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"testing"
)

func Test_StringToInt(t *testing.T) {
	value := "1000"

	handler := NewStringToIntHandler()

	data := models.NodeParams{
		"value": []byte(value),
	}

	postValues := handler.Handler.Execute(data)
	assert.Equal(t, uint(1000), postValues.GetUint("outValue"))
}

func Test_StringToIntWithError(t *testing.T) {
	value := "1x000"

	handler := NewStringToIntHandler()

	data := models.NodeParams{
		"value": []byte(value),
	}

	postValues := handler.Handler.Execute(data)
	assert.Equal(t, "failed-to-convert-string-to-int", postValues.GetError())
}
