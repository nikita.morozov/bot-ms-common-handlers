package handlers

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"testing"
)

func Test_Base64Encode(t *testing.T) {
	value := "Hello, 世界"
	handler := NewBase64EncodeHandler()

	data := models.NodeParams{
		"value": []byte(value),
	}

	resp := handler.Handler.Execute(data)

	assert.Equal(t, "SGVsbG8sIOS4lueVjA==", resp.GetString("outValue"))
}
