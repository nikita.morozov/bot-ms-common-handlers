package handlers

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"testing"
)

func Test_PrefixString(t *testing.T) {
	prefix := "add_test_prefix."
	value := "value_for_testing"

	handler := NewPrefixStringHandler()

	mockPostValues := []byte(prefix + value)

	data := models.NodeParams{
		"prefix": []byte(prefix),
		"value":  []byte(value),
	}

	postValues := handler.Handler.Execute(data)

	assert.Equal(t, mockPostValues, postValues["outValue"])
}

func Test_PrefixStringBreakLine(t *testing.T) {
	prefix := "\n"
	value := "value_for_testing"

	handler := NewPrefixStringHandler()

	mockPostValues := []byte(prefix + value)

	data := models.NodeParams{
		"prefix": []byte(prefix),
		"value":  []byte(value),
	}

	postValues := handler.Handler.Execute(data)

	assert.Equal(t, mockPostValues, postValues["outValue"])

	fmt.Printf("\n\x1b[32mPrefix String Test - Success\n")
}
