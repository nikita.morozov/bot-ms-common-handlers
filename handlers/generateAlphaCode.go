package handlers

import (
	"gitlab.com/nikita.morozov/bot-ms-common-handlers/tools"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type GenerateAlphaCodeHandler struct {
	min int
	max int
}

func (g *GenerateAlphaCodeHandler) Execute(params models.NodeParams) models.NodeData {
	count := params.GetInt("count")
	min := uint(g.min)
	max := uint(g.max)

	if count < min {
		count = min
	}
	if count > max {
		count = max
	}

	resp := models.NodeData{}

	code := tools.GetRandomAlphaCode(int(count))

	resp.Set("value", code)

	return resp.Complete(nil)
}

const GenerateAlphaCodeHandlerName = "core.generateAlphaCode"

func NewGenerateAlphaCodeHandler(min int, max int) models.ModuleHandler {
	return models.ModuleHandler{
		Name: GenerateAlphaCodeHandlerName,
		Handler: &GenerateAlphaCodeHandler{
			min: min,
			max: max,
		},
	}
}

func NewGenerateAlphaCodeAction() models.Action {
	return models.Action{
		Name:    "Core | Generate alpha code",
		Handler: GenerateAlphaCodeHandlerName,
		Sockets: []models.Socket{
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeEvent,
				Title:     "OnStart",
				Slug:      models.NodeDataOnStart,
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeInt,
				Title:     "Count",
				Slug:      "count",
			},
			// Out
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnComplete,
				Slug:      models.NodeDataOnComplete,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnError,
				Slug:      models.NodeDataOnError,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeString,
				Title:     "Value",
				Slug:      "value",
			},
		},
	}
}
