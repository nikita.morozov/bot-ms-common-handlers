package handlers

import (
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"strings"
)

type CheckStartWithHandler struct {
}

func (c *CheckStartWithHandler) Execute(params models.NodeParams) models.NodeData {
	value := params.GetString("value")
	startValue := params.GetString("startValue")

	if !strings.HasPrefix(value, startValue) {
		return models.NodeDataError("wrong-prefix")
	}

	return models.NodeDataComplete(nil)
}

const NewCheckStartWithHandlerName = "core.checkStartWith"

func NewCheckStartWithHandler() models.ModuleHandler {
	return models.ModuleHandler{
		Name:    NewCheckStartWithHandlerName,
		Handler: &CheckStartWithHandler{},
	}
}

func NewCheckStartWithAction() models.Action {
	return models.Action{
		Name:    "Core | Check string with",
		Handler: NewCheckStartWithHandlerName,
		Sockets: []models.Socket{
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeEvent,
				Title:     "OnStart",
				Slug:      models.NodeDataOnStart,
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Value",
				Slug:      "value",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Start with",
				Slug:      "startValue",
			},
			// Out
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnComplete,
				Slug:      models.NodeDataOnComplete,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnError,
				Slug:      models.NodeDataOnError,
			},
		},
	}
}
