package handlers

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"testing"
)

func Test_SuffixString(t *testing.T) {
	suffix := ".add_test_suffix"
	value := "value_for_testing"

	handler := NewSuffixStringHandler()

	mockPostValues := []byte(value + suffix)

	data := models.NodeParams{
		"suffix": []byte(suffix),
		"value":  []byte(value),
	}

	postValues := handler.Handler.Execute(data)

	assert.Equal(t, mockPostValues, postValues["outValue"])
}
