package handlers

import (
	"github.com/tidwall/sjson"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type AddToObjectHandler struct {
}

func (a *AddToObjectHandler) Execute(params models.NodeParams) models.NodeData {
	key := params.GetString("key")
	value := params.GetString("value")
	object := params.GetString("object")

	if key == "" {
		return models.NodeDataError("key-is-empty")
	}

	if object == "" {
		object = "{}"
	}

	val, err := sjson.Set(object, key, value)
	if err != nil {
		return models.NodeDataError(err.Error())
	}

	resp := models.NodeData{}

	resp.Set("outObject", val)
	return resp.Complete(nil)
}

const AddToObjectHandlerName = "core.addToObject"

func NewAddToObjectHandler() models.ModuleHandler {
	return models.ModuleHandler{
		Name:    AddToObjectHandlerName,
		Handler: &AddToObjectHandler{},
	}
}

func NewAddToObjectAction() models.Action {
	return models.Action{
		Name:    "Core | AddToObject",
		Handler: AddToObjectHandlerName,
		Sockets: []models.Socket{
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeEvent,
				Title:     "OnStart",
				Slug:      models.NodeDataOnStart,
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Object",
				Slug:      "object",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Value",
				Slug:      "value",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Key",
				Slug:      "key",
			},
			// Out
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnComplete,
				Slug:      models.NodeDataOnComplete,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnError,
				Slug:      models.NodeDataOnError,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeString,
				Title:     "Object",
				Slug:      "outObject",
			},
		},
	}
}
