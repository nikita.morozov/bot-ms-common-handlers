package handlers

import (
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type SuffixStringHandler struct {
}

func (s *SuffixStringHandler) Execute(params models.NodeParams) models.NodeData {
	suffix := params.GetString("suffix")
	value := params.GetString("value")

	result := value + suffix

	resp := models.NodeData{}

	resp.Set("outValue", result)

	return resp.Complete(nil)
}

const SuffixStringHandlerName = "core.suffixString"

func NewSuffixStringHandler() models.ModuleHandler {
	return models.ModuleHandler{
		Name:    SuffixStringHandlerName,
		Handler: &SuffixStringHandler{},
	}
}

func NewSuffixStringAction() models.Action {
	return models.Action{
		Name:    "Core | Suffix String",
		Handler: SuffixStringHandlerName,
		Sockets: []models.Socket{
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeEvent,
				Title:     "OnStart",
				Slug:      models.NodeDataOnStart,
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Suffix",
				Slug:      "suffix",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Value",
				Slug:      "value",
			},
			// Out
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnComplete,
				Slug:      models.NodeDataOnComplete,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnError,
				Slug:      models.NodeDataOnError,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeString,
				Title:     "Value",
				Slug:      "outValue",
			},
		},
	}
}
