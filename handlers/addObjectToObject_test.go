package handlers

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"testing"
)

func Test_AddObjectToObject(t *testing.T) {
	key := "params"
	value := "{\"from\":10,\"to\":20}"
	object := "{\"key\":\"test\"}"
	result := "{\"key\":\"test\",\"params\":{\"from\":10,\"to\":20}}"

	handler := NewAddObjectToObjectHandler()

	data := models.NodeParams{
		"key":    []byte(key),
		"value":  []byte(value),
		"object": []byte(object),
	}

	postValues := handler.Handler.Execute(data)

	assert.Equal(t, []byte(result), postValues["outObject"])
}

func BenchmarkAddObjectToObject(b *testing.B) {
	key := "params"
	value := "{\"from\": 10, \"to\": 20}"
	object := "{\"key\": \"test\"}"

	handler := NewAddObjectToObjectHandler()

	data := models.NodeParams{
		"key":    []byte(key),
		"value":  []byte(value),
		"object": []byte(object),
	}

	for i := 0; i < b.N; i++ {
		handler.Handler.Execute(data)
	}
}
