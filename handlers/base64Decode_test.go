package handlers

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"testing"
)

func Test_Base64Decode(t *testing.T) {
	value := "SGVsbG8sIOS4lueVjA=="
	handler := NewBase64DecodeHandler()

	data := models.NodeParams{
		"value": []byte(value),
	}

	resp := handler.Handler.Execute(data)

	assert.Equal(t, "Hello, 世界", resp.GetString("outValue"))
}
