package handlers

import (
	"bytes"
	"encoding/json"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"text/template"
)

type TemplateStringHandler struct {
}

func (p *TemplateStringHandler) Execute(params models.NodeParams) models.NodeData {
	value := params.GetString("value")
	values := params.GetString("values")

	var dat map[string]interface{}
	err := json.Unmarshal([]byte(values), &dat)

	tmpl, err := template.New("test").Parse(value)
	if err != nil {
		return models.NodeDataError(err.Error())
	}

	buf := new(bytes.Buffer)
	err = tmpl.Execute(buf, dat)

	resp := models.NodeData{}
	resp.Set("outValue", buf.String())
	return resp.Complete(nil)
}

const TemplateStringHandlerName = "core.template-string"

func NewTemplateStringHandler() models.ModuleHandler {
	return models.ModuleHandler{
		Name:    TemplateStringHandlerName,
		Handler: &TemplateStringHandler{},
	}
}

func NewTemplateStringAction() models.Action {
	return models.Action{
		Name:    "Core | Template String",
		Handler: TemplateStringHandlerName,
		Sockets: []models.Socket{
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeEvent,
				Title:     "OnStart",
				Slug:      models.NodeDataOnStart,
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Value",
				Slug:      "value",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Values",
				Slug:      "values",
			},

			// Out
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnComplete,
				Slug:      models.NodeDataOnComplete,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnError,
				Slug:      models.NodeDataOnError,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeString,
				Title:     "Value",
				Slug:      "outValue",
			},
		},
	}
}
