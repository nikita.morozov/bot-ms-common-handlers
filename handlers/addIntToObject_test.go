package handlers

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"testing"
)

func Test_AddIntToObject(t *testing.T) {
	key := "value_key"
	value := "10"
	object := "{\"items\":[{\"command\":\"command1\",\"scenarioId\":\"1\"},{\"command\":\"command2\",\"scenarioId\":\"2\"}]}"
	result := "{\"items\":[{\"command\":\"command1\",\"scenarioId\":\"1\"},{\"command\":\"command2\",\"scenarioId\":\"2\"}],\"value_key\":10}"

	handler := NewAddIntToObjectHandler()

	data := models.NodeParams{
		"key":    []byte(key),
		"value":  []byte(value),
		"object": []byte(object),
	}

	postValues := handler.Handler.Execute(data)

	assert.Equal(t, []byte(result), postValues["outObject"])
}

func BenchmarkAddIntToObject(b *testing.B) {
	key := "value_key"
	value := "10"
	object := "{\"items\": [{\"command\": \"command1\", \"scenarioId\": \"1\"},\n{\"command\": \"command2\", \"scenarioId\": \"2\"}]}"

	handler := NewAddIntToObjectHandler()

	data := models.NodeParams{
		"key":    []byte(key),
		"value":  []byte(value),
		"object": []byte(object),
	}

	for i := 0; i < b.N; i++ {
		handler.Handler.Execute(data)
	}
}
