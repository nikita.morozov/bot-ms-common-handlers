package handlers

import (
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type CheckStringLengthHandler struct {
}

func (c *CheckStringLengthHandler) Execute(params models.NodeParams) models.NodeData {
	value := params.GetString("value")
	length := params.GetUint("length")

	if len(value) != int(length) {
		return models.NodeDataError("length-not-equal")
	}

	return models.NodeDataComplete(nil)
}

const NewCheckStringLengthHandlerName = "core.checkStringLength"

func NewCheckStringLengthHandler() models.ModuleHandler {
	return models.ModuleHandler{
		Name:    NewCheckStringLengthHandlerName,
		Handler: &CheckStringLengthHandler{},
	}
}

func NewCheckStringLengthAction() models.Action {
	return models.Action{
		Name:    "Core | Check string length",
		Handler: NewCheckStringLengthHandlerName,
		Sockets: []models.Socket{
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeEvent,
				Title:     "OnStart",
				Slug:      models.NodeDataOnStart,
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Value",
				Slug:      "value",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeInt,
				Title:     "Length",
				Slug:      "length",
			},
			// Out
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnComplete,
				Slug:      models.NodeDataOnComplete,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnError,
				Slug:      models.NodeDataOnError,
			},
		},
	}
}
