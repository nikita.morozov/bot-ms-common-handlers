package handlers

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"testing"
)

func Test_TemplateString(t *testing.T) {
	value := "💰 Your balance of {{.token}} tokens {{.missing}} available for withdrawal is {{.masterAQXUser1}} {{.token}}.\n\n$1 is required to complete the operation. You have $10 on your account with jetup.org cabinets.\n\nEnter the number of tokens to withdraw if you wish to continue."
	values := "{\"token\":\"AQX\",\"masterAQXUser1\":12,\"bonusAQXUser1\":10}"

	handler := NewTemplateStringHandler()

	mockPostValues := "💰 Your balance of AQX tokens <no value> available for withdrawal is 12 AQX.\n\n$1 is required to complete the operation. You have $10 on your account with jetup.org cabinets.\n\nEnter the number of tokens to withdraw if you wish to continue."

	data := models.NodeParams{
		"value":  []byte(value),
		"values": []byte(values),
	}

	postValues := handler.Handler.Execute(data)

	assert.Equal(t, string(postValues["outValue"]), mockPostValues)
}
