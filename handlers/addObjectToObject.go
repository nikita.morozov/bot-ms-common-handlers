package handlers

import (
	"github.com/tidwall/gjson"
	"github.com/tidwall/sjson"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type AddObjectToObjectHandler struct {
}

func (a *AddObjectToObjectHandler) Execute(params models.NodeParams) models.NodeData {
	key := params.GetString("key")
	value := params.GetString("value")
	object := params.GetString("object")

	if key == "" {
		return models.NodeDataError("key-is-empty")
	}

	if object == "" {
		object = "{}"
	}

	m, ok := gjson.Parse(value).Value().(map[string]interface{})
	if !ok {
		return models.NodeDataError("value-error")
	}

	val, err := sjson.Set(object, key, m)
	if err != nil {
		return models.NodeDataError(err.Error())
	}

	resp := models.NodeData{}

	resp.Set("outObject", val)
	return resp.Complete(nil)
}

const AddObjectToObjectHandlerName = "core.addObjectToObject"

func NewAddObjectToObjectHandler() models.ModuleHandler {
	return models.ModuleHandler{
		Name:    AddObjectToObjectHandlerName,
		Handler: &AddObjectToObjectHandler{},
	}
}

func NewAddObjectToObjectAction() models.Action {
	return models.Action{
		Name:    "Core | AddObjectToObject",
		Handler: AddObjectToObjectHandlerName,
		Sockets: []models.Socket{
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeEvent,
				Title:     "OnStart",
				Slug:      models.NodeDataOnStart,
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Object",
				Slug:      "object",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Value",
				Slug:      "value",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Key",
				Slug:      "key",
			},
			// Out
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnComplete,
				Slug:      models.NodeDataOnComplete,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnError,
				Slug:      models.NodeDataOnError,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeString,
				Title:     "Object",
				Slug:      "outObject",
			},
		},
	}
}
