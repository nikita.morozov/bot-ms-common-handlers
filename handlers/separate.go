package handlers

import (
	"fmt"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"strings"
)

type SeparateHandler struct {
}

func (s *SeparateHandler) Execute(params models.NodeParams) models.NodeData {
	separator := params.GetString("separator")
	value := params.GetString("value")

	result := strings.Split(value, separator)

	if separator == "" || value == "" {
		return models.NodeDataError("SEPARATOR OR VALUE IS NULL OR EMPTY")
	}

	resp := models.NodeData{}

	secondArr := append(result[1:])
	secondStr := fmt.Sprintf(`%v`, secondArr)
	secondStr = strings.Replace(secondStr, "[", "", -1)
	secondStr = strings.Replace(secondStr, "]", "", -1)
	secondStr = strings.Trim(secondStr, " ")

	resp.Set("value1", result[0])
	resp.Set("value2", secondStr)

	return resp.Complete(nil)
}

const SeparateHandlerName = "core.separate"

func NewSeparateHandler() models.ModuleHandler {
	return models.ModuleHandler{
		Name:    SeparateHandlerName,
		Handler: &SeparateHandler{},
	}
}

func NewSeparateAction() models.Action {
	return models.Action{
		Name:    "Core | String separate",
		Handler: SeparateHandlerName,
		Sockets: []models.Socket{
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeEvent,
				Title:     "OnStart",
				Slug:      models.NodeDataOnStart,
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Separator",
				Slug:      "separator",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Value",
				Slug:      "value",
			},
			// Out
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnComplete,
				Slug:      models.NodeDataOnComplete,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnError,
				Slug:      models.NodeDataOnError,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeString,
				Title:     "Value 1",
				Slug:      "value1",
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeString,
				Title:     "Value 2",
				Slug:      "value2",
			},
		},
	}
}
