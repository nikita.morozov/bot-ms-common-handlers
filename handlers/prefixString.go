package handlers

import (
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type PrefixStringHandler struct {
}

func (p *PrefixStringHandler) Execute(params models.NodeParams) models.NodeData {
	prefix := params.GetString("prefix")
	value := params.GetString("value")

	result := prefix + value

	resp := models.NodeData{}

	resp.Set("outValue", result)

	return resp.Complete(nil)
}

const PrefixStringHandlerName = "core.prefixString"

func NewPrefixStringHandler() models.ModuleHandler {
	return models.ModuleHandler{
		Name:    PrefixStringHandlerName,
		Handler: &PrefixStringHandler{},
	}
}

func NewPrefixStringAction() models.Action {
	return models.Action{
		Name:    "Core | Prefix String",
		Handler: PrefixStringHandlerName,
		Sockets: []models.Socket{
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeEvent,
				Title:     "OnStart",
				Slug:      models.NodeDataOnStart,
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Prefix",
				Slug:      "prefix",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Value",
				Slug:      "value",
			},
			// Out
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnComplete,
				Slug:      models.NodeDataOnComplete,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnError,
				Slug:      models.NodeDataOnError,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeString,
				Title:     "Value",
				Slug:      "outValue",
			},
		},
	}
}
