package handlers

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"testing"
)

func Test_AddToObject(t *testing.T) {
	key := "value_key"
	value := "some_value"
	object := "{\"items\":[{\"command\":\"command1\",\"scenarioId\":\"1\"},{\"command\":\"command2\",\"scenarioId\":\"2\"}]}"
	result := "{\"items\":[{\"command\":\"command1\",\"scenarioId\":\"1\"},{\"command\":\"command2\",\"scenarioId\":\"2\"}],\"value_key\":\"some_value\"}"

	handler := NewAddToObjectHandler()

	data := models.NodeParams{
		"key":    []byte(key),
		"value":  []byte(value),
		"object": []byte(object),
	}

	postValues := handler.Handler.Execute(data)

	assert.Equal(t, []byte(result), postValues["outObject"])
}

func Test_AddToObjectEmptyObject(t *testing.T) {
	key := "value_key"
	value := "some_value"
	object := ""
	result := "{\"value_key\":\"some_value\"}"

	handler := NewAddToObjectHandler()

	data := models.NodeParams{
		"key":    []byte(key),
		"value":  []byte(value),
		"object": []byte(object),
	}

	postValues := handler.Handler.Execute(data)

	assert.Equal(t, []byte(result), postValues["outObject"])
}

func Test_AddToObjectDepthKey(t *testing.T) {
	key := "value_key.val"
	value := "some_value"
	object := ""
	result := "{\"value_key\":{\"val\":\"some_value\"}}"

	handler := NewAddToObjectHandler()

	data := models.NodeParams{
		"key":    []byte(key),
		"value":  []byte(value),
		"object": []byte(object),
	}

	postValues := handler.Handler.Execute(data)

	assert.Equal(t, []byte(result), postValues["outObject"])
}

func BenchmarkAddToObject(b *testing.B) {
	key := "value_key"
	value := "some_value"
	object := "{\"items\": [{\"command\": \"command1\", \"scenarioId\": \"1\"},\n{\"command\": \"command2\", \"scenarioId\": \"2\"}]}"

	handler := NewAddToObjectHandler()

	data := models.NodeParams{
		"key":    []byte(key),
		"value":  []byte(value),
		"object": []byte(object),
	}

	for i := 0; i < b.N; i++ {
		handler.Handler.Execute(data)
	}
}
