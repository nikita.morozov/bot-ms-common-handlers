package handlers

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"strconv"
	"testing"
)

func Test_GenerateAlphaCode(t *testing.T) {
	count := 10

	handler := NewGenerateAlphaCodeHandler(2, 10)

	data := models.NodeParams{
		"count": []byte(strconv.Itoa(count)),
	}

	resp := handler.Handler.Execute(data)

	assert.NotNil(t, string(resp["value"]))

	fmt.Printf("\n\x1b[32mGenerate Alpha Code Test - Success\n")
}
