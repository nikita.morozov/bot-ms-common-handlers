package handlers

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"testing"
)

func Test_PrecisionConvert(t *testing.T) {
	handler := NewPrecisionConvertHandler()

	mockPostValues := []byte("10")

	data := models.NodeParams{
		"precision": []byte("2"),
		"value":     []byte("1000"),
	}

	postValues := handler.Handler.Execute(data)
	assert.Equal(t, mockPostValues, postValues["outValue"])
}

func Test_PrecisionConvert2(t *testing.T) {
	handler := NewPrecisionConvertHandler()

	mockPostValues := []byte("10.01")

	data := models.NodeParams{
		"precision": []byte("2"),
		"value":     []byte("1001"),
	}

	postValues := handler.Handler.Execute(data)
	assert.Equal(t, mockPostValues, postValues["outValue"])
}

func Test_PrecisionConvert3(t *testing.T) {
	handler := NewPrecisionConvertHandler()

	mockPostValues := []byte("0.01")

	data := models.NodeParams{
		"precision": []byte("2"),
		"value":     []byte("1"),
	}

	postValues := handler.Handler.Execute(data)
	assert.Equal(t, mockPostValues, postValues["outValue"])
}

func Test_PrecisionConvert4(t *testing.T) {
	handler := NewPrecisionConvertHandler()

	mockPostValues := []byte("0")

	data := models.NodeParams{
		"precision": []byte("2"),
		"value":     []byte("0"),
	}

	postValues := handler.Handler.Execute(data)
	assert.Equal(t, mockPostValues, postValues["outValue"])
}
