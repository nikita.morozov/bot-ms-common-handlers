package handlers

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"testing"
)

func Test_CheckStartWith(t *testing.T) {
	value := "0x0032427854397435"
	startWith := "0x"

	handler := NewCheckStartWithHandler()

	data := models.NodeParams{
		"value":     []byte(value),
		"startWith": []byte(startWith),
	}

	resp := handler.Handler.Execute(data)

	assert.Equal(t, "true", resp.GetString(models.NodeDataOnComplete))
}

func BenchmarkCheckStartWith(b *testing.B) {
	value := "0x0032427854397435"
	startWith := "0x"

	handler := NewCheckStartWithHandler()

	data := models.NodeParams{
		"value":     []byte(value),
		"startWith": []byte(startWith),
	}

	for i := 0; i < b.N; i++ {
		handler.Handler.Execute(data)
	}
}
