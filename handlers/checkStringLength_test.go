package handlers

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"testing"
)

func Test_CheckStringLength(t *testing.T) {
	value := "0x0032427854397435"
	length := "18"

	handler := NewCheckStringLengthHandler()

	data := models.NodeParams{
		"value":  []byte(value),
		"length": []byte(length),
	}

	resp := handler.Handler.Execute(data)

	assert.Equal(t, "true", resp.GetString(models.NodeDataOnComplete))
}
