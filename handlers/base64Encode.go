package handlers

import (
	"encoding/base64"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type Base64EncodeHandler struct {
}

func (c *Base64EncodeHandler) Execute(params models.NodeParams) models.NodeData {
	value := params.GetString("value")

	resp := models.NodeData{}

	encoded := base64.StdEncoding.EncodeToString([]byte(value))
	resp.Set("outValue", encoded)

	return resp.Complete(nil)
}

const NewBase64EncodeHandlerName = "core.base64Encode"

func NewBase64EncodeHandler() models.ModuleHandler {
	return models.ModuleHandler{
		Name:    NewBase64EncodeHandlerName,
		Handler: &Base64EncodeHandler{},
	}
}

func NewBase64EncodeAction() models.Action {
	return models.Action{
		Name:    "Coder | Base64Encode",
		Handler: NewBase64EncodeHandlerName,
		Sockets: []models.Socket{
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeEvent,
				Title:     "OnStart",
				Slug:      models.NodeDataOnStart,
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Value",
				Slug:      "value",
			},
			// Out
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnComplete,
				Slug:      models.NodeDataOnComplete,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnError,
				Slug:      models.NodeDataOnError,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeString,
				Title:     "Value",
				Slug:      "outValue",
			},
		},
	}
}
