package handlers

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"testing"
)

func Test_Condition(t *testing.T) {
	a := "a"
	b := ""

	handler := NewConditionHandler()

	data := models.NodeParams{
		"a": []byte(a),
		"b": []byte(b),
	}

	resp := handler.Handler.Execute(data)

	assert.Equal(t, "true", resp.GetString("false"))

	fmt.Printf("\n\x1b[32mCondition Test - Success\n")
}
