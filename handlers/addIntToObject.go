package handlers

import (
	"github.com/tidwall/sjson"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type AddIntToObjectHandler struct {
}

func (a *AddIntToObjectHandler) Execute(params models.NodeParams) models.NodeData {
	key := params.GetString("key")
	value := params.GetInt("value")
	object := params.GetString("object")

	if key == "" {
		return models.NodeDataError("key-is-empty")
	}

	if object == "" {
		object = "{}"
	}

	val, err := sjson.Set(object, key, value)
	if err != nil {
		return models.NodeDataError(err.Error())
	}

	resp := models.NodeData{}

	resp.Set("outObject", val)
	return resp.Complete(nil)
}

const AddIntToObjectHandlerName = "core.addIntToObject"

func NewAddIntToObjectHandler() models.ModuleHandler {
	return models.ModuleHandler{
		Name:    AddIntToObjectHandlerName,
		Handler: &AddIntToObjectHandler{},
	}
}

func NewAddIntToObjectAction() models.Action {
	return models.Action{
		Name:    "Core | AddIntToObject",
		Handler: AddIntToObjectHandlerName,
		Sockets: []models.Socket{
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeEvent,
				Title:     "OnStart",
				Slug:      models.NodeDataOnStart,
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Object",
				Slug:      "object",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeInt,
				Title:     "Value",
				Slug:      "value",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Key",
				Slug:      "key",
			},
			// Out
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnComplete,
				Slug:      models.NodeDataOnComplete,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnError,
				Slug:      models.NodeDataOnError,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeString,
				Title:     "Object",
				Slug:      "outObject",
			},
		},
	}
}
