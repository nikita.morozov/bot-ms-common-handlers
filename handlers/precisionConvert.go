package handlers

import (
	"fmt"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"math"
	"strconv"
)

type PrecisionConvertHandler struct {
}

func (s *PrecisionConvertHandler) Execute(params models.NodeParams) models.NodeData {
	value := params.GetString("value")
	precision := params.GetUint("precision")

	floatValue, err := strconv.ParseFloat(value, 64)
	if err != nil {
		return models.NodeDataError(err.Error())
	}

	resValue := floatValue / math.Pow10(int(precision))

	resp := models.NodeData{}

	resp.Set("outValue", fmt.Sprintf("%g", resValue))
	return resp.Complete(nil)
}

const PrecisionConvertHandlerName = "core.precisionConvert"

func NewPrecisionConvertHandler() models.ModuleHandler {
	return models.ModuleHandler{
		Name:    PrecisionConvertHandlerName,
		Handler: &PrecisionConvertHandler{},
	}
}

func NewPrecisionConvertAction() models.Action {
	return models.Action{
		Name:    "Core | Precision Convert",
		Handler: PrecisionConvertHandlerName,
		Sockets: []models.Socket{
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeEvent,
				Title:     "OnStart",
				Slug:      models.NodeDataOnStart,
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Value",
				Slug:      "value",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeInt,
				Title:     "Precision",
				Slug:      "precision",
			},
			// Out
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnComplete,
				Slug:      models.NodeDataOnComplete,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnError,
				Slug:      models.NodeDataOnError,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeString,
				Title:     "Value",
				Slug:      "outValue",
			},
		},
	}
}
