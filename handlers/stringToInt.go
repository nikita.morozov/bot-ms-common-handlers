package handlers

import (
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"strconv"
)

type StringToIntHandler struct {
}

func (s *StringToIntHandler) Execute(params models.NodeParams) models.NodeData {
	value := params.GetString("value")

	intValue, err := strconv.ParseInt(value, 10, 32)
	if err != nil {
		return models.NodeDataError("failed-to-convert-string-to-int")
	}

	resp := models.NodeData{}
	resp.SetInt("outValue", int(intValue))
	return resp.Complete(nil)
}

const StringToIntHandlerName = "core.stringToInt"

func NewStringToIntHandler() models.ModuleHandler {
	return models.ModuleHandler{
		Name:    StringToIntHandlerName,
		Handler: &StringToIntHandler{},
	}
}

func NewStringToIntAction() models.Action {
	return models.Action{
		Name:    "Core | String to int",
		Handler: StringToIntHandlerName,
		Sockets: []models.Socket{
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeEvent,
				Title:     "OnStart",
				Slug:      models.NodeDataOnStart,
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Value",
				Slug:      "value",
			},
			// Out
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnComplete,
				Slug:      models.NodeDataOnComplete,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnError,
				Slug:      models.NodeDataOnError,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeInt,
				Title:     "Value",
				Slug:      "outValue",
			},
		},
	}
}
