package handlers

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"testing"
)

func Test_Separate(t *testing.T) {
	separator := "_"
	value := "value_for_testing"
	result := "for testing"

	handler := NewSeparateHandler()

	data := models.NodeParams{
		"separator": []byte(separator),
		"value":     []byte(value),
	}

	postValues := handler.Handler.Execute(data)

	assert.Equal(t, "value", string(postValues["value1"]))
	assert.Equal(t, result, string(postValues["value2"]))

	fmt.Printf("\n\x1b[32mSeparate Test - Success\n")
}
