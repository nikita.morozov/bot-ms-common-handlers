package handlers

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"testing"
)

func Test_CompareNumber(t *testing.T) {
	value1 := "10"
	value2 := "2"
	comparison := ">"

	handler := NewCompareNumberHandler()

	data := models.NodeParams{
		"value1":     []byte(value1),
		"value2":     []byte(value2),
		"comparison": []byte(comparison),
	}

	resp := handler.Handler.Execute(data)

	assert.Equal(t, "true", resp.GetString("true"))
}

func Test_CompareNumberUnknownSign(t *testing.T) {
	value1 := "10"
	value2 := "2"
	comparison := "!"

	handler := NewCompareNumberHandler()

	data := models.NodeParams{
		"value1":     []byte(value1),
		"value2":     []byte(value2),
		"comparison": []byte(comparison),
	}

	resp := handler.Handler.Execute(data)

	assert.Equal(t, "comparison-operator-not-found", resp.GetError())
}
