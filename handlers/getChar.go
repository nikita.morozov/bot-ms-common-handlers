package handlers

import (
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"strconv"
)

type GetCharHandler struct {
}

func (g *GetCharHandler) Execute(params models.NodeParams) models.NodeData {
	indexInt := params.GetInt("index")
	value := params.GetString("value")

	index, indexErr := strconv.ParseUint(strconv.Itoa(int(indexInt)), 10, 32)
	if indexErr != nil || value == "" {
		return models.NodeDataError("INDEX OR VALUE IS NULL OR EMPTY")
	}

	runeValue := []rune(value)

	result := string(runeValue[index : index+1])

	resp := models.NodeData{}

	resp.Set("outValue", result)

	return resp.Complete(nil)
}

const GetCharHandlerName = "core.getChar"

func NewGetCharHandler() models.ModuleHandler {
	return models.ModuleHandler{
		Name:    GetCharHandlerName,
		Handler: &GetCharHandler{},
	}
}

func NewGetCharAction() *models.Action {
	return &models.Action{
		Name:    "Core | Get character",
		Handler: GetCharHandlerName,
		Sockets: []models.Socket{
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeEvent,
				Title:     "OnStart",
				Slug:      models.NodeDataOnStart,
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeInt,
				Title:     "Index",
				Slug:      "index",
			},
			{
				Direction: models.SocketDirectionIn,
				Type:      models.SocketTypeString,
				Title:     "Value",
				Slug:      "value",
			},
			// Out
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnComplete,
				Slug:      models.NodeDataOnComplete,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeEvent,
				Title:     models.NodeDataOnError,
				Slug:      models.NodeDataOnError,
			},
			{
				Direction: models.SocketDirectionOut,
				Type:      models.SocketTypeString,
				Title:     "Value",
				Slug:      "outValue",
			},
		},
	}
}
