package methodHandlers

import (
	"bytes"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
	"io"
	"net/http"
	"strings"
)

type httpRequestHandler struct {
}

func (h *httpRequestHandler) Execute(params models.NodeParams) models.NodeData {
	urlPath := params.GetString("url")
	method := params.GetString("method")
	contentType := params.GetString("contentType")

	resp := models.NodeData{}

	if contentType == "" {
		contentType = "application/json"
	}

	var err error
	var response *http.Response
	if strings.ToUpper(method) == "POST" {
		data := params.GetString("data")
		response, err = http.Post(urlPath, contentType, bytes.NewBuffer([]byte(data)))
	}
	response, err = http.Get(urlPath)
	if err != nil {
		return resp.Error(err.Error())
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		return resp.Error(err.Error())
	}

	resp.Set("response", string(body))
	resp.Set("status", response.Status)

	return resp.Complete(nil)
}

func NewHttpRequestHandler() models.IHandler {
	return &httpRequestHandler{}
}
