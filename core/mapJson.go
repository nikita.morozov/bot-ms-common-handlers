package methodHandlers

import (
	"github.com/tidwall/gjson"
	"gitlab.com/nikita.morozov/bot-ms-core/models"
)

type mapJsonHandler struct {
}

func (h *mapJsonHandler) Execute(params models.NodeParams) models.NodeData {
	data := params.GetString("data")
	path := params.GetString("path")

	value := gjson.Get(data, path)

	resp := models.NodeData{}

	resp.Set("value", value.String())

	return resp.Complete(nil)
}

func NewMapJsonHandler() models.IHandler {
	return &mapJsonHandler{}
}
